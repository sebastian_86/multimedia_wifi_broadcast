# multimedia_wifi_broadcast

This program is targeting to "broadcast" multimedia from Wi-Fi adapter using 802.11AC NIC
The selected NIC chips is RTL8812AU, a USB NIC, driver can get from [here](https://github.com/astsam/rtl8812au)

## Contain of this work

1. A transmitter program "tx"
2. A receiver program "rx"

## How it works!

1. Assuming the driver of NIC has already compiled and installed
2. Compile it using the provided Makefile, tx and rx program will be generated
3. Initialize the broadcast environment by usign the provided script, wbcinit.sh
4. For multimedia streaming, FFMPEG is required
5. Streaming broadcast: ffmpeg -re -stream_loop -1 -i <video file> -codec copy -f mpegts pipe:1 | ./tx -b 1 -f 1450 -y 0 -t 1 -d 9 <NIC name>
6. Receiver: ./rx -b 1 -r 0 -f 1450 -t 1 <NIC name>
7. Parameter of transmitter and receiver must be same
