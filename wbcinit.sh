#!/bin/bash

# Change the network interface name as ifconfig shown
NIC=wlx88d7f6022d2f

sudo ifconfig $NIC down
sudo iw dev $NIC set monitor otherbss fcsfail
sudo ifconfig $NIC up

# frequency maybe vary based on which channel and bandwidth you want to use
sudo iw dev $NIC set freq 5580 80 5610
