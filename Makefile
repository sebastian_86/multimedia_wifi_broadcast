LDFLAGS=-lrt -lpcap
CPPFLAGS=-Wall -D _GNU_SOURCE -O2
all: rx tx

%.o: %.c
	$(CC) -c -o $@ $< $(CPPFLAGS)


rx: rx.o lib.o radiotap.o fec.o
	$(CC) -o $@ $^ $(LDFLAGS)


tx: tx.o lib.o fec.o
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f rx tx *~ *.o
